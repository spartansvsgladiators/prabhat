-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2015 at 01:49 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `cse_paper_variable`
--

CREATE TABLE IF NOT EXISTS `cse_paper_variable` (
  `paper_id` varchar(255) NOT NULL DEFAULT 'CSE000',
  `no_of_question` int(255) NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cse_paper_variable`
--

INSERT INTO `cse_paper_variable` (`paper_id`, `no_of_question`, `status`) VALUES
('1', 0, '0'),
('pkm0021', 4, '1'),
('pkm0022', 4, '0'),
('pkm0023', 0, '0'),
('pkm0031', 3, '1'),
('pkm0032', 2, '1'),
('pkm0033', 0, '0'),
('pkmt0011', 5, '0'),
('pkmt0012', 2, '0'),
('pkmt0013', 3, '0'),
('pkmt0014', 2, '1'),
('pkmt0015', 4, '1'),
('pkmt0016', 4, '0'),
('pkmt0017', 1, '1'),
('pkmt0018', 3, '1'),
('pkmt0019', 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `cse_question`
--

CREATE TABLE IF NOT EXISTS `cse_question` (
  `paper_id` varchar(255) NOT NULL DEFAULT 'CSE000',
  `question_no` varchar(255) NOT NULL,
  `option1` varchar(255) NOT NULL,
  `option2` varchar(255) NOT NULL,
  `option3` varchar(255) NOT NULL,
  `option4` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
`counter` bigint(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `cse_question`
--

INSERT INTO `cse_question` (`paper_id`, `question_no`, `option1`, `option2`, `option3`, `option4`, `answer`, `counter`) VALUES
('pkmt0011', '<p>gfffgh</p>\r\n', 'dfdgfd', 'dfdfdg', 'retretr', 'etreert', '2', 69),
('pkmt0011', '<p>nbnbmbbmn</p>\r\n', 'rretretre', 'esf', 'cfgf', 'cxbvxc', '4', 70),
('pkmt0011', '<p>ghjggjh</p>\r\n', 'ewerwer', 'reyruy', 'fxgfdgf', 'cvcbnvn', '1', 71),
('pkmt0011', '<p>hghgjhg</p>\r\n', 'ererer', 'retre', 'fdgd', 'dfddg', '2', 72),
('pkmt0011', '<p>ghghghj</p>\r\n', 'ewwtre', 'werwe', 'ewsdfs', 'dsfsfd', '3', 73),
('pkmt0012', '<p>fggfhg</p>\r\n', 'wewre', 'edffgd', 'fhjghj', 'cvbcvb', '4', 74),
('pkmt0012', '<p>bvnvnvc</p>\r\n', 'ewewer', 'ewwydfc', 'djf', 'kh', '1', 75),
('pkmt0013', '<p>fgffhg</p>\r\n', 'wreer', 'rytrty', 'sadsf', 'fhgfjf', '1', 76),
('pkmt0013', '<p>uiouiuio</p>\r\n', 'weww', 'tyuttu', 'xsdfsfds', 'jgghgj', '3', 77),
('pkmt0013', '<p>fghgjhgj</p>\r\n', 'ewewr', 'dsdfds', 'czxczc', 'bmbnm', '4', 78),
('pkmt0014', '<p>fgfgfg</p>\r\n', 'werwewe', 'ryryqtghfh', 'gfdhd', 'euyrt', '2', 79),
('pkmt0014', '<p>dgfds</p>\r\n', 'wqweqe', 'erwy', 'reetu', 'dsfg', '4', 80),
('pkmt0015', '<p>fgfhfgh</p>\r\n', 'ewewer', 'ewrewre', 'ewewre', 'rewrewre', '2', 81),
('pkmt0015', '<p>vvbvmngjh</p>\r\n', 'ewreue', 'bvcvcbv', 'cxxv', 'bcxvxvx', '1', 82),
('pkmt0015', '<p>fghffhg</p>\r\n', 'utuyyoi', 'qewyr', 'xvccvb', 'ioppipoipo', '3', 83),
('pkmt0015', '<p>ytyryriyk</p>\r\n', 'rewewer', 'rewrwerew', 'sssxcsgd', 'vcmvbn', '4', 84),
('pkm0021', '<p>gffgj</p>\r\n', 'qwqrew', 'wteewt', 'dsfsdf', 'hdfdhg', '2', 85),
('pkm0021', '<p>fghghkj</p>\r\n', 'rereyt', 'fdgh', 'zvcxcx', 'rhuj', '1', 86),
('pkm0021', '<p>hklj</p>\r\n', 'wew', 'sgdds', 'xczvc', 'yyi', '4', 87),
('pkm0021', '<p>gkjmb</p>\r\n', 'ewer', 'dsgf', 'xbxbc', ',mbb', '3', 88),
('pkm0022', '<p>hjkhjhhkj</p>\r\n', 'nmn,m', 'nmn,mn', 'nmn,mn', 'bmnbmn', '2', 89),
('pkm0022', '<p>nm,nmn,m</p>\r\n', ' n,mn', 'uui', 'werwre', 'ewerwr', '4', 90),
('pkm0022', '<p>urtyer</p>\r\n', 'ewewtr', 'wweer', 'dfgdgq', 'dvdhv', '1', 91),
('pkm0022', '<p>bmnb,m</p>\r\n', 'ewerwre', 'wewtre', 'dssg', 'xfhdx', '3', 92),
('pkmt0016', '<p>xcx</p>\r\n', 'rere', 'fdf', 'fdf', 'xcx', '1', 93),
('pkmt0016', '<p>jhkjhkjh</p>\r\n', 'jhjh', 'jhkjh', 'kjhkj', 'hkjh', '3', 94),
('pkmt0016', '<p>lkjkljlkj</p>\r\n', 'jlkjklj', 'kjlkjklj', 'lkjkljlkj', 'kljlkjkl', '2', 95),
('pkmt0016', '<p>jlkjklj</p>\r\n', 'kjkljlkj', 'lkjkljlj', 'kjkljkl', 'jlkj', '4', 96),
('pkmt0017', '<p>sasds</p>\r\n', 'jhkjh', 'jhkjhk', 'kjhkjhkj', 'jkhkjh2', '2', 97),
('pkmt0018', '<p>kjkjkj</p>\r\n', 'jkj', 'kjkj', 'kjkj', 'kjkjk', '2', 98),
('pkmt0018', '<p>jhjkhjkh</p>\r\n', 'jhjhjkh', 'jhj,m', 'nmnmbnnm', 'bnbnbnm', '1', 99),
('pkmt0018', '<p>jkkljlkjkl</p>\r\n', 'kjkljkljlk', 'kj,mnmn', 'm,nmnnbnmbnb', 'nmbnbmnbnm', '4', 100),
('pkmt0019', '<p>kkljklj</p>\r\n', 'kjjlkjlkj', 'lkjklj', 'kjjlkjkljlkj', 'm,mnm,', '1', 101),
('pkmt0019', '<p>jkljlkjlk</p>\r\n', 'jkljkljklj', 'kj,mnm,nm,n', 'nmbnnbbv', 'nbnvbnvbn', '3', 102),
('pkmt0019', '<p>kjjkjkljlk</p>\r\n', 'jhjkhjkjh', 'qggqyyugq', 'vcvbcbvc', 'bvbnvbnvbn', '2', 103),
('pkm0031', '<p>dgdg</p>\r\n', 'dg', 'dg', 'dg', 'dg', '3', 104),
('pkm0031', '<p>rwrw</p>\r\n', 'ef', 'fsz', 'sf', 'fr', '2', 105),
('pkm0031', '<p>gdg</p>\r\n', 'rw', 'df', 'gd', 'gd', '4', 106),
('pkm0032', '<p>hfh</p>\r\n', 'fgh', 'fgh', 'fg', 'f', '2', 107),
('pkm0032', '<p>sfs</p>\r\n', 'dg', 'dg', 'dg', 'dg', '1', 108);

-- --------------------------------------------------------

--
-- Table structure for table `cse_test`
--

CREATE TABLE IF NOT EXISTS `cse_test` (
  `teacher_id` varchar(255) NOT NULL,
  `paper_id` varchar(255) NOT NULL,
  `paper_title` varchar(255) NOT NULL,
  `time` datetime(6) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `time_required` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cse_test`
--

INSERT INTO `cse_test` (`teacher_id`, `paper_id`, `paper_title`, `time`, `description`, `time_required`) VALUES
('', '1', '', '0000-00-00 00:00:00.000000', 'bkb', 30),
('pkm002', 'pkm0021', 'Hello1', '2015-04-02 00:00:00.000000', 'kiuio', 20),
('pkm002', 'pkm0022', 'Hello2', '2015-04-03 23:37:14.352540', 'yvuy', 30),
('pkm002', 'pkm0023', 'Hello3', '2015-03-12 00:21:32.623000', 'yfuy', 30),
('pkm003', 'pkm0031', 'hello13', '2015-04-03 16:01:40.000000', 'vgui', 30),
('pkm003', 'pkm0032', 'hello14', '2015-04-03 16:02:04.000000', 'fyfu', 30),
('pkm003', 'pkm0033', '', '0000-00-00 00:00:00.000000', '', 0),
('pkmt001', 'pkmt0011', 'Hello4', '0000-00-00 00:00:00.000000', 'fuyfu', 30),
('pkmt001', 'pkmt0012', 'Hello5', '0000-00-00 00:00:00.000000', 'fuf', 30),
('pkmt001', 'pkmt0013', 'Hello6', '0000-00-00 00:00:00.000000', 'guygi', 30),
('pkmt001', 'pkmt0014', 'Hello7', '0000-00-00 00:00:00.000000', 'bguigiu', 30),
('pkmt001', 'pkmt0015', 'Hello8', '0000-00-00 00:00:00.000000', 'giug', 30),
('pkmt001', 'pkmt0016', 'hello9', '0000-00-00 00:00:00.000000', 'gigolki', 30),
('pkmt001', 'pkmt0017', 'hello10', '0000-00-00 00:00:00.000000', 'erst', 30),
('pkmt001', 'pkmt0018', 'hello11', '1970-01-01 01:00:00.000000', 'dtrd', 30),
('pkmt001', 'pkmt0019', 'hello12', '2015-04-03 13:05:30.000000', 'tydy', 30);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `department_name` varchar(255) NOT NULL,
  `department_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE IF NOT EXISTS `register` (
`id` bigint(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `roll_no` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `gender` enum('0','1') NOT NULL DEFAULT '0',
  `semester` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `activation` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `username`, `email`, `roll_no`, `password`, `department`, `gender`, `semester`, `date_of_birth`, `profile_photo`, `activation`) VALUES
(12, 'hghjgjhgjhg', 'ewewew@dfdf.ghj', 'nbhjhg', '25f9e794323b453885f5181f1b624d0b', 'cse', '1', 'werwrew', '0555-05-05', '', '1'),
(13, 'qwerty', 'qwer@qw.com', 'qwer', '25f9e794323b453885f5181f1b624d0b', 'ece', '0', 'eee', '0344-12-12', '', '1'),
(14, 'james', 'peter@gmail.com', 'b12cs001', '25f9e794323b453885f5181f1b624d0b', 'cse', '0', '5', '2015-03-10', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
`serial_no` bigint(20) NOT NULL,
  `paper_id` varchar(255) NOT NULL,
  `student_roll_no` varchar(255) NOT NULL,
  `score` int(255) NOT NULL,
  `no` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`serial_no`, `paper_id`, `student_roll_no`, `score`, `no`) VALUES
(20, 'pkm0022', 'b12cs001', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_detail`
--

CREATE TABLE IF NOT EXISTS `teacher_detail` (
  `teacher_id` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `department_id` varchar(255) NOT NULL,
  `no_of_paper` int(255) NOT NULL DEFAULT '0',
  `department` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_detail`
--

INSERT INTO `teacher_detail` (`teacher_id`, `email`, `name`, `password`, `department_id`, `no_of_paper`, `department`) VALUES
('pkm002', 'qwerty@gmail.com', 'Deepak kumar', '827ccb0eea8a706c4c34a16891f84e7b', 'cse1', 3, 'cse'),
('pkm003', 'ray@gmail.com', 'r ray', '827ccb0eea8a706c4c34a16891f84e7b', 'cse1', 3, 'cse'),
('pkmt001', 'hello@gmail.com', 'Manjish paul', '827ccb0eea8a706c4c34a16891f84e7b', 'ece1', 9, 'ece');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cse_paper_variable`
--
ALTER TABLE `cse_paper_variable`
 ADD PRIMARY KEY (`paper_id`);

--
-- Indexes for table `cse_question`
--
ALTER TABLE `cse_question`
 ADD PRIMARY KEY (`counter`);

--
-- Indexes for table `cse_test`
--
ALTER TABLE `cse_test`
 ADD PRIMARY KEY (`paper_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `roll_no` (`roll_no`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
 ADD PRIMARY KEY (`serial_no`);

--
-- Indexes for table `teacher_detail`
--
ALTER TABLE `teacher_detail`
 ADD PRIMARY KEY (`teacher_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cse_question`
--
ALTER TABLE `cse_question`
MODIFY `counter` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
MODIFY `serial_no` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
